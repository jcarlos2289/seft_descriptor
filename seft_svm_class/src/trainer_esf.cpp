#include <ros/ros.h>
#include "ros/package.h"
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <limits>
#include <pcl/conversions.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <pcl/features/esf.h>
#include <string>
#include <cstdlib>
#include "std_msgs/String.h"
//include <pcl/ml/svm.h>
#include "svm/svm.h"
#include "fileMethods/fileMethods.h"
#include <pcl/features/vfh.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <flann/flann.hpp>
#include <boost/random.hpp>
#include <iostream>
#include <iomanip>

std::vector<std::string> split(std::string s, char delim)
{
  std::stringstream ss(s);
  std::string item;
  std::vector<std::string> tokens;
  while (getline(ss, item, delim))
  {
    tokens.push_back(item);
  }
  return tokens;
}

int main(int argc, char **argv)
{
  if (argc < 3)
  {
    std::cerr << "Usage: " << argv[0] << "<Trainng List path file> <Descriptor Type> <Id_Test>" << std::endl;
    return 1;
  }

  std::string trainPath = argv[1];
  std::string desc = argv[2];
  std::string idTest = argv[3];
  std::cout << "Experimento # " << idTest << std::endl;
  // ROS init
  ros::init(argc, argv, "svm_trainer");
  boost::shared_ptr<ros::NodeHandle> nh_ptr_;
  nh_ptr_ = boost::make_shared<ros::NodeHandle>();
  ros::Publisher svm_chat_pub = nh_ptr_->advertise<std_msgs::String>("svm_chat", 1000);
  /*
-s svm_type : set type of SVM (default 0)
	0 -- C-SVC
	1 -- nu-SVC
	2 -- one-class SVM
	3 -- epsilon-SVR
	4 -- nu-SVR
-t kernel_type : set type of kernel function (default 2)
	0 -- linear: u'*v
	1 -- polynomial: (gamma*u'*v + coef0)^degree
	2 -- radial basis function: exp(-gamma*|u-v|^2)
	3 -- sigmoid: tanh(gamma*u'*v + coef0)
-d degree : set degree in kernel function (default 3)
-g gamma : set gamma in kernel function (default 1/num_features)
-r coef0 : set coef0 in kernel function (default 0)
-c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
-n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
-p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
-m cachesize : set cache memory size in MB (default 100)
-e epsilon : set tolerance of termination criterion (default 0.001)
-h shrinking: whether to use the shrinking heuristics, 0 or 1 (default 1)
-b probability_estimates: whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
-wi weight: set the parameter C of class i to weight*C, for C-SVC (default 1)*/

  //svm_model_ptr svmModel;
  svm_parameter svmParameters;
  svmParameters.svm_type = C_SVC;
  svmParameters.kernel_type = RBF;
  svmParameters.degree = 3;

  svmParameters.gamma = 0.5; /////////////////////////////////2e-1;

  svmParameters.coef0 = 0; // para los kernel con coef
  svmParameters.nu = 0.5;  // Solo para los que usen nu
  svmParameters.cache_size = 100;

  svmParameters.C = 2048; ///////////////////////////////2e9;

  svmParameters.eps = 1e-3; // solo para el svm de tipo epsilon
  svmParameters.p = 0.1;
  svmParameters.shrinking = 1;
  svmParameters.probability = 0;
  svmParameters.nr_weight = 0;
  svmParameters.weight_label = NULL;
  svmParameters.weight = NULL;
  svmParameters.probability = 1;

  struct svm_problem problem;
  struct svm_model *model;

  pcl::PointCloud<pcl::ESFSignature640>::Ptr descriptor(new pcl::PointCloud<pcl::ESFSignature640>());
  std::string line;

  std::ifstream myfile(trainPath.c_str()); //"/home/jcarlos2289/Documentos/seftDataset/DescriptorsList/ESF_Train.txt"); //ESF_Tag_Obj
  //std::ifstream myfile( "/home/jcarlos2289/Documentos/seftDatasetTest/ESF_test_Tag_Obj.txt");

  myfile.unsetf(std::ios_base::skipws);

  // count the newlines with an algorithm specialized for counting:
  unsigned line_count = std::count(
      std::istream_iterator<char>(myfile),
      std::istream_iterator<char>(),
      '\n');
  myfile.clear();
  myfile.seekg(0, std::ios::beg);
  std::cout << "Lines: " << line_count << "\n";

  int trainingWords = (int)line_count;

  problem.l = trainingWords;
  problem.y = new double[problem.l];

  std::string::size_type sz; // alias of size_t

  svm_node **x_node = new svm_node *[problem.l];

  //guardar los valores de train
  std::stringstream trainDataString;

  //guardar la lista de etiquetas en el train y reales
  std::stringstream trainLabels;
  std::stringstream realLabels;
  realLabels   << "Label"  << "\n";
  trainLabels << "Label"
              << "\n";

  float gamma = 0.0f;
  if (myfile.is_open())
  {
    int i = 0;
    int dictionarySize = descriptor->points[0].descriptorSize();
    //gamma = 1 / dictionarySize;

    while (getline(myfile, line))
    {
      std::vector<std::string> data;
      data = split(line, '\t');
      pcl::io::loadPCDFile(data.at(0), *descriptor);

      int label = std::atoi(data.at(1).c_str());
      dictionarySize = descriptor->points[0].descriptorSize();

      x_node[i] = new svm_node[dictionarySize + 1];
      //guardar los valores de train
      trainDataString << label << " ";
      trainLabels << label << "\n";
      realLabels << data.at(2) << "\n";
      for (int j = 0; j < dictionarySize; j++)
      {
        x_node[i][j].index = j + 1;
        x_node[i][j].value = descriptor->points[0].histogram[j];
        //guardar los valores de train
        trainDataString << j + 1 << ":" << descriptor->points[0].histogram[j] << " ";
      }
      //guardar los valores de train
      trainDataString << "\n";
      x_node[i][dictionarySize].index = -1;
      problem.y[i] = label;
      i++;

    } //end while
    problem.x = x_node;
    myfile.close();
  }

  fileMethods::saveData("/home/jcarlos2289/catkin_ws/results/seft_svm_class/modelTrainDistribution_" + idTest, trainLabels.str(), false);
  fileMethods::saveData("/home/jcarlos2289/catkin_ws/results/seft_svm_class/modelTrainRealDistribution_" + idTest, realLabels.str(), false);

  // svmParameters.gamma = gamma;
  std::string modelPath = ros::package::getPath("seft_svm_class");

  //para guardar los valores de los atributos de train
  /*std::stringstream nameFile;
   nameFile<< modelPath << "/models/TrainVectorData_"<< idTest <<".txt";
    std::ofstream fs2(nameFile.str().c_str(), std::ofstream::out);
    fs2 << trainDataString.str() << std::endl;
    fs2.close();*/

  modelPath += "/models/svmModel_esf_" + idTest + ".txt";

  std::cout << modelPath << std::endl;

  model = svm_train(&problem, &svmParameters);
  printf("Result:%d", svm_save_model(modelPath.c_str(), model));

  std_msgs::String msg;

  std::stringstream ss;
  ss << modelPath;

  msg.data = ss.str();

  svm_chat_pub.publish(msg);

  // new lines will be skipped unless we stop it from happening:

  //std::cout << "Lines: " << line_count << "\n";
  ROS_INFO("Training Finished.");
  // Return control to ROS
  //ros::spin();

  //return 0;
}