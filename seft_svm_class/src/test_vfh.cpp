#include <ros/ros.h>
#include "ros/package.h"
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <limits>
#include <pcl/conversions.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <pcl/features/esf.h>
#include <string>
#include <cstdlib>
#include "std_msgs/String.h"
//include <pcl/ml/svm.h>
#include "svm/svm.h"
#include <pcl/features/vfh.h>

std::string testPath;
std::string desc;

std::vector<std::string> split(std::string s, char delim)
{
  std::stringstream ss(s);
  std::string item;
  std::vector<std::string> tokens;
  while (getline(ss, item, delim))
  {
    tokens.push_back(item);
  }
  return tokens;
}

void chatterCallback(const std_msgs::String::ConstPtr &msg)
{
  ROS_INFO("I heard the signal.");

  struct svm_problem problem;
  struct svm_model *model;

  
  
  pcl::PointCloud<pcl::VFHSignature308>::Ptr descriptor(new pcl::PointCloud<pcl::VFHSignature308>());


  std::string line;
  //std::ifstream myfile( "/home/jcarlos2289/Documentos/seftDataset/ESF_Tag_Obj.txt");
  std::ifstream myfile(testPath.c_str()); //"/home/jcarlos2289/Documentos/seftDataset/DescriptorsList/ESF_Test.txt");

  myfile.unsetf(std::ios_base::skipws);

  // count the newlines with an algorithm specialized for counting:
  unsigned line_count = std::count(
      std::istream_iterator<char>(myfile),
      std::istream_iterator<char>(),
      '\n');
  myfile.clear();
  myfile.seekg(0, std::ios::beg);
  //std::cout << "Lines: " << line_count << "\n";

  int testWords = (int)line_count;

  //problem.l = trainingWords;
  //	problem.y = new double[problem.l];

  std::string::size_type sz; // alias of size_t
  std::vector<int> origLables;

  svm_node **x_node = new svm_node *[testWords];

  if (myfile.is_open())
  {
    int i = 0;
    while (getline(myfile, line))
    {
      std::vector<std::string> data;
      data = split(line, '\t');

      pcl::io::loadPCDFile(data.at(0), *descriptor);
      int label = std::atoi(data.at(1).c_str());

      //float r = 0.0f;
      int dictionarySize = descriptor->points[0].descriptorSize();
      //for (int j = 0; j < descriptor->points[0].descriptorSize(); j++)
      // {
      //r += descriptor->points[0].histogram[j];
      x_node[i] = new svm_node[dictionarySize + 1];
      for (int j = 0; j < dictionarySize; j++)
      {
        x_node[i][j].index = j + 1;
        x_node[i][j].value = descriptor->points[0].histogram[j];
      }
      x_node[i][dictionarySize].index = -1;
      //} //end for
      origLables.push_back(label);
      i++;
    } //end while
    // problem.x = x_node;
    myfile.close();
  }

  float numFails = 0.0;
  float numHits = 0.0;

  //std::string modelPath = ros::package::getPath("seft_svm_class");
  //modelPath += "/models/svmModel.txt";
  std::string modelPath;
  modelPath = msg->data.c_str();
  model = svm_load_model(modelPath.c_str());
  std::stringstream predictedObjects;
  std::stringstream filePredictName;
  filePredictName << "/home/jcarlos2289/catkin_ws/results/seft_svm_class/VFH_Predicted_Objects.txt";
  predictedObjects << "Real;Predicted\n";

  for (int i = 0; i < testWords; ++i)
  {

    int retvalBoW = svm_predict(model, x_node[i]);
     predictedObjects << origLables.at(i) << ";" << retvalBoW << "\n";
    if (retvalBoW == origLables.at(i))
      numHits += 1.0;
    else
    {
      numFails += 1.0;
      std::cout << "Test frame " << i << " - Real Object: " << origLables.at(i) << " - Estimated Object: " << retvalBoW << std::endl;
    }
  }

  printf("Percentage of hits:%2.2f and fails :%2.2f\n", 100.0 * (numHits / (numHits + numFails)), 100.0 * (numFails / (numHits + numFails)));

  std::cout << std::endl;

 std::ofstream fs(filePredictName.str().c_str(), std::ofstream::out); //"/home/jcarlos2289/catkin_ws/results/seft_pipeline/esf_.txt"

  fs << predictedObjects.str() << std::endl;
  fs.close();


  ROS_INFO("Test Finished =).");
  ros::shutdown();
}

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    std::cerr << "Usage: " << argv[0] << "<Test List path file> <Descriptor Type>" << std::endl;
    return 1;
  }

  testPath = argv[1];
  desc = argv[2];

  // ROS init
  ros::init(argc, argv, "svm_test");
  boost::shared_ptr<ros::NodeHandle> nh_ptr_;
  nh_ptr_ = boost::make_shared<ros::NodeHandle>();
  ros::Subscriber sub = nh_ptr_->subscribe("svm_chat", 1000, chatterCallback);

  // Return control to ROS
  ros::spin();

  return 0;
}