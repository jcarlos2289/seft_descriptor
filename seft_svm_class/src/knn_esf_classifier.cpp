#include <ros/ros.h>
#include "ros/package.h"
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <limits>
#include <pcl/conversions.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <pcl/features/esf.h>
#include <string>
#include <cstdlib>
#include "std_msgs/String.h"
//include <pcl/ml/svm.h>
//#include "svm/svm.h"
#include <pcl/features/vfh.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <flann/flann.hpp>
#include <boost/random.hpp>
#include <iostream>
#include <iomanip>


std::vector<std::string> split(std::string s, char delim)
{
  std::stringstream ss(s);
  std::string item;
  std::vector<std::string> tokens;
  while (getline(ss, item, delim))
  {
    tokens.push_back(item);
  }
  return tokens;
}

int main(int argc, char **argv)
{
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << "<Trainng List path file> <Descriptor Type>" << std::endl;
    return 1;
  }

  std::string trainPath = argv[1];
  std::string desc = argv[2];

  // ROS init
  ros::init(argc, argv, "knn_classifier");
  boost::shared_ptr<ros::NodeHandle> nh_ptr_;
  nh_ptr_ = boost::make_shared<ros::NodeHandle>();
  //ros::Publisher svm_chat_pub = nh_ptr_->advertise<std_msgs::String>("svm_chat", 1000);

  pcl::PointCloud<pcl::ESFSignature640>::Ptr descriptor(new pcl::PointCloud<pcl::ESFSignature640>());
  std::string line;

  std::ifstream myfile(trainPath.c_str());//"/home/jcarlos2289/Documentos/seftDataset/DescriptorsList/ESF_Train.txt"); //ESF_Tag_Obj
  //std::ifstream myfile( "/home/jcarlos2289/Documentos/seftDatasetTest/ESF_test_Tag_Obj.txt");

  myfile.unsetf(std::ios_base::skipws);

  // count the newlines with an algorithm specialized for counting:
  unsigned line_count = std::count(
      std::istream_iterator<char>(myfile),
      std::istream_iterator<char>(),
      '\n');
  myfile.clear();
  myfile.seekg(0, std::ios::beg);
  std::cout << "Lines: " << line_count << "\n";

  int trainingWords = (int)line_count;

 // problem.l = trainingWords;
//  problem.y = new double[problem.l];

  std::string::size_type sz; // alias of size_t

 std::vector<float*> trainFeatures;
 std::vector<int> classes;
int dictionarySize ;
  if (myfile.is_open())
  {
    int i = 0;
    dictionarySize = descriptor->points[0].descriptorSize();
    //gamma = 1 / dictionarySize;

    while (getline(myfile, line))
    {
      std::vector<std::string> data;
      data = split(line, '\t');
      pcl::io::loadPCDFile(data.at(0), *descriptor);

      int label = std::atoi(data.at(1).c_str());
      dictionarySize = descriptor->points[0].descriptorSize();
      std::vector<float> tr_file;
        
      for (int j = 0; j < dictionarySize; j++)
      {        
        tr_file.push_back(descriptor->points[0].histogram[j]);
      }
      
      classes.push_back(label);
     

    } //end while
   
    myfile.close();
  }

  //[cantidad de nubes de train * dimensionalidad del descriptor], cantidad de nubes de train, dimensionalidd
  flann::Matrix<float> data (new float[trainFeatures.size() * dictionarySize], trainFeatures.size(), dictionarySize);
  for (size_t i = 0; i < data.rows; ++i)
	    for (size_t j = 0; j < data.cols; ++j)
	      data[i][j] = trainFeatures.at(i)[j];

flann::Index<flann::ChiSquareDistance<float> >* index;
index = new flann::Index<flann::ChiSquareDistance<float> >(data, flann::KDTreeIndexParams (4));
index->buildIndex ();
std::cout << "Index Construido" << std::endl;
 // svmParameters.gamma = gamma;

 // std::string modelPath = ros::package::getPath("seft_svm_class");
  //modelPath += "/models/svmModel_esf.txt";

  //std::cout << modelPath << std::endl;

  //model = svm_train(&problem, &svmParameters);
  //printf("Result:%d", svm_save_model(modelPath.c_str(), model));

  //std_msgs::String msg;

 // std::stringstream ss;
 // ss << modelPath;

 // msg.data = ss.str();

 // svm_chat_pub.publish(msg);

  // new lines will be skipped unless we stop it from happening:

  //std::cout << "Lines: " << line_count << "\n";
  ROS_INFO("Training Finished.");
  // Return control to ROS
  //ros::spin();

  //return 0;
}