#include <ros/ros.h>
#include "ros/package.h"
#include <geometry_msgs/Point.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include "seft_pipeline/cluster_clouds.h"
#include "seft_pipeline/tagService.h"
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <limits>
#include <pcl/conversions.h>
#include <image_transport/image_transport.h>
#include <opencv/cv.h>
#include <image_geometry/pinhole_camera_model.h>
#include <tf/transform_listener.h>
#include <boost/foreach.hpp>
#include <sensor_msgs/image_encodings.h>
#include "seft_pipeline/cluster_data.h"
#include <pcl/io/pcd_io.h>
#include <pcl/features/esf.h>
#include "seft_pipeline/cluster_data.h"
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_types.h>
#include <pcl/features/vfh.h>
#include <iostream>
#include <fstream>
#include <iostream>
#include <fstream>

ros::Publisher cluster_pub_;
ros::Publisher cluster_ind_pub_;
ros::Subscriber rgbdimage_sub_;
boost::shared_ptr<ros::NodeHandle> nh_ptr_;
ros::ServiceClient client;
sensor_msgs::CameraInfo camInfo;
ros::Subscriber camera_sub;
int sendCounter;
std::string object;

void cameraInfoCallBack(const sensor_msgs::CameraInfoConstPtr &input)
{

  camInfo = *input;
  ROS_INFO("Camera Parameters Saved");
  //camera_sub.shutdown();

} //sensor_msgs::PointCloud2ConstPt

// Single RGB-D image processing
void rgbdImageCallback(const sensor_msgs::PointCloud2ConstPtr &in_cloud)
{

  ROS_INFO("Received point cloud.");
  //camera_sub = nh_ptr_->subscribe("/camera/rgb/camera_info", 1, cameraInfoCallBack);
  //rgbdimage_sub_.shutdown();

 static const std::string arr[] = {"remote_control", "carton", "espresso_maker", "electric_guitar", "acoustic_guitar",
                                      "joystick", "hair_spray", "ocarina", "computer_keyboard", "space_bar", "typewritter_keyboard",
                                      "violin", "cello", "Loafer", "maraca", "tennis_ball"};
 
  std::vector<std::string> tagsToDescribe(arr, arr + sizeof(arr) / sizeof(arr[0]));

  // Convert Point Cloud
  pcl::PCLPointCloud2 pc2_cloud;
  pcl_conversions::toPCL(*in_cloud, pc2_cloud);

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudNoNaNs(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::fromPCLPointCloud2(pc2_cloud, *cloud);

  // Convert RGB image
  sensor_msgs::ImagePtr in_img(new sensor_msgs::Image);
  pcl::toROSMsg(*in_cloud, *in_img);
  cv_bridge::CvImageConstPtr imgOriginal = cv_bridge::toCvShare(in_img, sensor_msgs::image_encodings::BGR8);
  /// cv::imwrite( "/home/jcarlos2289/Documentos/GridedImages/Original.jpg", imgOriginal->image );

  std::vector<int> indices;
  pcl::removeNaNFromPointCloud(*cloud, *cloudNoNaNs, indices);

  //--------------------------Clustering procedure---------------------------------
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_f(new pcl::PointCloud<pcl::PointXYZRGB>);
  // std::cout << "PointCloud before filtering has: " << cloud->points.size () << " data points." << std::endl; //*

  // Create the filtering object: downsample the dataset using a leaf size of 1cm
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGB>);

  pcl::VoxelGrid<pcl::PointXYZRGB> vg;
  vg.setInputCloud(cloud);
  vg.setLeafSize(0.005f, 0.005f, 0.05f); //0.01f
  vg.filter(*cloud_filtered);
  // std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl; //*

  //cloud_filtered = cloud;
  //cloud_filtered= cloudNoNaNs;

  //cloud_filtered->width = cloud->width*cloud->height;
  //cloud_filtered->height = 1;
  // Create the segmentation object for the planar model and set all the parameters
  pcl::SACSegmentation<pcl::PointXYZRGB> seg;
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane(new pcl::PointCloud<pcl::PointXYZRGB>());
  pcl::PCDWriter writer;
  seg.setOptimizeCoefficients(true);
  seg.setModelType(pcl::SACMODEL_PLANE);
  seg.setMethodType(pcl::SAC_RANSAC);
  seg.setMaxIterations(100);
  seg.setDistanceThreshold(0.02);

  int i = 0, nr_points = (int)cloud_filtered->points.size();
  while (cloud_filtered->points.size() > 0.15 * nr_points)
  {
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud(cloud_filtered);
    seg.segment(*inliers, *coefficients);
    if (inliers->indices.size() == 0)
    {
      //std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
      break;
    }

    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    extract.setInputCloud(cloud_filtered);
    extract.setIndices(inliers);
    extract.setNegative(false);

    // Get the points associated with the planar surface
    extract.filter(*cloud_plane);
    // std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

    // Remove the planar inliers, extract the rest
    extract.setNegative(true);
    extract.filter(*cloud_f);
    *cloud_filtered = *cloud_f;
  }

  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>);
  tree->setInputCloud(cloud_filtered);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
  ec.setClusterTolerance(0.015); // 2cm
  ec.setMinClusterSize(100);
  ec.setMaxClusterSize(25000);
  ec.setSearchMethod(tree);
  ec.setInputCloud(cloud_filtered);
  ec.extract(cluster_indices);

  std::vector<pcl::PointCloud<pcl::PointXYZRGB> > clustersDetected;

  int j = 0;
  for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
  {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZRGB>);

    for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
      cloud_cluster->points.push_back(cloud_filtered->points[*pit]); // it -> indices, es un std vector de enteros por lo cual cada pcl::PointIndices es un stdvector
                                                                     //y por lo tanto cluster_indices es un stdvector de stdvector<int>
    cloud_cluster->width = cloud_cluster->points.size();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;
    clustersDetected.push_back(*cloud_cluster);
    // std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
    //std::stringstream ss;
    //ss << "/home/jcarlos2289/catkin_ws/results/seft_pipeline/cloud_cluster_" << j << ".pcd";
    // writer.write<pcl::PointXYZRGB> (ss.str (), *cloud_cluster, false); //se guarda la nube en la carpeta
    j++;
  }
  std::stringstream sl;
  sl << "Cluster Detected: " << clustersDetected.size() << std::endl;
 // std::cout << "Clustering Procedure finished !!!! " << std::endl;
  std::cout << sl.str();

  //------------------------------end Clustering Procedure------------------------

  //----------------------------------centroid calculation------------------------

  // Create and accumulate points

  std::vector<pcl::PointXYZ> centroids;
  std::vector<sensor_msgs::PointCloud2> clusterVector;
  for (std::vector<pcl::PointCloud<pcl::PointXYZRGB> >::iterator it = clustersDetected.begin(); it != clustersDetected.end(); ++it)
  {
    pcl::CentroidPoint<pcl::PointXYZ> centroid;
    for (int i = 0; i < it->points.size(); ++i)
    {
      centroid.add(
          pcl::PointXYZ(it->points[i].x,
                        it->points[i].y,
                        it->points[i].z));
    }
    pcl::PointXYZ c1;
    centroid.get(c1);
    centroids.push_back(c1);
    sensor_msgs::PointCloud2 in_img;
    pcl::toROSMsg(*it, in_img);
    clusterVector.push_back(in_img);
  }
  std::stringstream sla;
  sla << "Centroids Detected: " << centroids.size() << std::endl;

  std::vector<geometry_msgs::Point> positions;

  for (int i = 0; i < centroids.size(); ++i)
  {
    sla << "\nCluster #: " << i << "\n"
        << " " << centroids.at(i).x
        << " " << centroids.at(i).y
        << " " << centroids.at(i).z << std::endl;
    geometry_msgs::Point p;
    p.x = centroids.at(i).x;
    p.y = centroids.at(i).y;
    p.z = centroids.at(i).z;
    positions.push_back(p);
  }
  // std::cout << sla.str() <<std::endl;

  //------------------------------------------------------------------------------

  //---------------------Procesamiento de los clusters en Caffe----------------------
  seft_pipeline::tagService srv;
  std::vector<seft_pipeline::Prediction> predictionsVector;
  //sensor_msgs::PointCloud2 <- clusterVector
  //pcl::PointCloud<pcl::PointXYZRGB> <-clustersDetected, vector

  std::vector<geometry_msgs::Point> positions_filtered;
  std::vector<sensor_msgs::PointCloud2> clusterVector_filtered;
  //std::vector<CV::Mat> cvMatVector;

  for (int i = 0; i < clusterVector.size(); ++i)
  {

    //-----------------**----------**----------**-------------**--------------**-----------**----------**------------------

    //-----------------**----------**----------**-------------**--------------**-----------**----------**------------------
    //cv_bridge::toCvShare(in_img, sensor_msgs::image_encodings::BGR8);
    //obtener el min y max de los puntos y luego buscar con KDTree
    //el punto maás cercano a ese en la nube raw y obtener el indice de dicho punto

    pcl::PointXYZRGB min_pt;
    pcl::PointXYZRGB max_pt;

    pcl::PointCloud<pcl::PointXYZRGB> ramCloud; //(new pcl::PointCloud<pcl::PointXYZRGB>);
    ramCloud = clustersDetected.at(i);

    pcl::getMinMax3D(ramCloud, min_pt, max_pt);

    image_geometry::PinholeCameraModel cam_model_;

    cam_model_.fromCameraInfo(camInfo);

    cv::Point3d pt_cv_min(min_pt.x, min_pt.y, min_pt.z);
    cv::Point2d uv_min;
    uv_min = cam_model_.project3dToPixel(pt_cv_min);

    cv::Point3d pt_cv_max(max_pt.x, max_pt.y, max_pt.z);
    cv::Point2d uv_max;
    uv_max = cam_model_.project3dToPixel(pt_cv_max);
    //comprobar que los minimos sean los valores menores en la pareja min-max
    float ram = 0.0;

    if (uv_min.x > uv_max.x)
    {
      ram = uv_min.x;
      uv_min.x = uv_max.x;
      uv_max.x = ram;
    }

    ram = 0.0;
    if (uv_min.y > uv_max.y)
    {
      ram = uv_min.y;
      uv_min.y = uv_max.y;
      uv_max.y = ram;
    }

    std::stringstream sd;
    sd << "\n\nCoordinates of Min and Max in 2D\n"
       << "Min " << uv_min.x << "  " << uv_min.y << std::endl
       << "Max " << uv_max.x << "  " << uv_max.y << std::endl;
    //std::cout << sd.str() <<std::endl;

    int minFadding, sizePadding;
    int cloudWidth, cloudHeight;
    minFadding = 15;
    sizePadding = 25;
    cloudWidth = cloud->width;
    cloudHeight = cloud->height;

    double width, height;
    std::stringstream sl;
    sl << "Min " << min_pt.x << "\t" << min_pt.y << "\t" << min_pt.z << std::endl
       << "Max " << max_pt.x << "\t" << max_pt.y << "\t" << max_pt.z << std::endl;
    width = max_pt.x - min_pt.x;
    height = max_pt.y - min_pt.y;

    //std::cout << sl.str() <<std::endl;
    if (uv_min.x > 0 && uv_min.y > 0)
    {
      positions_filtered.push_back(positions.at(i));
      clusterVector_filtered.push_back(clusterVector.at(i));
      /*  cv::Mat ram = imgOriginal->image;
       cv::circle(ram, uv_max, 3, CV_RGB(255,0,0), -1);
       cv::circle(ram, uv_min, 3, CV_RGB(0,255,0), -1);

       cv::imwrite("/home/jcarlos2289/Documentos/GridedImages/Original.jpg", ram );*/

      if ((uv_max.x + sizePadding) > cloudWidth)
      {
        width = cloudWidth - sizePadding - uv_min.x;
      }
      else
        width = uv_max.x - uv_min.x;

      if ((uv_max.y + sizePadding) > cloudHeight)
        height = cloudHeight - sizePadding - uv_min.y;
      else
        height = uv_max.y - uv_min.y;

      cv::Point2d cutPoint;
      if ((uv_min.x - minFadding) < 0)
        cutPoint.x = 0;
      else
        cutPoint.x = uv_min.x - minFadding;

      if ((uv_min.y - minFadding) < 0)
        cutPoint.y = 0;
      else
        cutPoint.y = uv_min.y - minFadding;

      float w, h;
      if (width + sizePadding > cloudWidth - 1)
        w = cloudWidth - 1;
      else
        w = width + sizePadding;

      if (height + sizePadding > cloudHeight - 1)
        h = cloudHeight - 1;
      else
        h = height + sizePadding;

      std::stringstream preCut;

      preCut << "Valores para recorte\n"
             << "X " << (uv_min.x - minFadding) << "\tY " << (uv_min.y - minFadding) << std::endl
             << "W " << w << "\tH " << h << std::endl
             << "\nValores para recorte Nuevos\n"
             << "X " << cutPoint.x << "\tY " << cutPoint.y << std::endl
             << "W " << w << "\tH " << h << std::endl;

      // std::cout << preCut.str()<< std::endl;
      try
      {

        cv::Rect rect = cv::Rect(cutPoint.x, cutPoint.y, w, h);
        cv::Mat cutImage = cv::Mat(imgOriginal->image, rect);
        cv::Mat copyImage = cutImage;

        if (width > height)
        {
          cv::Mat ram1, ram2, outImg;
          /* cv::Size size(width +sizePadding,width +sizePadding);
           resize(copyImage,outImg,size);*/

          int inc = ((width + sizePadding) - (height + sizePadding)) / 2;
          if (inc <= 0)
          {
            inc += 1;
          }
          ram1.create(inc, width + sizePadding, CV_8UC3); //rows cols
          ram1.setTo(cv::Scalar(0, 0, 0));                //255,255,255
          cv::vconcat(ram1, copyImage, ram2);
          cv::vconcat(ram2, ram1, outImg);

          cutImage = outImg;
        }

        if (width < height)
        {
          cv::Mat ram1, ram2, outImg;
          /* cv::Size size(height +sizePadding,height +sizePadding);
           resize(copyImage,outImg,size);*/

          int inc = ((height + sizePadding) - (width + sizePadding)) / 2;
          if (inc <= 0)
          {
            inc += 1;
          }
          ram1.create(height + sizePadding, inc, CV_8UC3); //rows cols
          ram1.setTo(cv::Scalar(0, 0, 0));
          cv::hconcat(ram1, copyImage, ram2);
          cv::hconcat(ram2, ram1, outImg);

          cutImage = outImg;
        }

        //---------------------------*************************----------------------------***************************************
        // cv::imwrite( ss.str(), imgClus->image );
        //------------------------Esto esta bien se usa el srv de ROS------------------------

        //Esto tranforma de un cvMAT a un sensor_msg Image img->image para obtener la imagen MAT
        sensor_msgs::ImagePtr msg2;
        msg2 = cv_bridge::CvImage(std_msgs::Header(), sensor_msgs::image_encodings::BGR8, cutImage).toImageMsg();
        msg2->encoding = sensor_msgs::image_encodings::BGR8;
        // *msg2 es lo q enviaria a roscaffe despues de q agrego el fondo

        //esto si
        srv.request.image = *msg2;
        if (client.call(srv))
        {
          seft_pipeline::Prediction msgRam;

          predictionsVector.push_back(srv.response.predictions);
        }
        else
        {
          ROS_ERROR("Failed to call service tag_Service");
          //return 1;
        }
        //---------------------------*************************----------------------------***************************************

        if (std::find(tagsToDescribe.begin(), tagsToDescribe.end(), srv.response.predictions.label[0]) != tagsToDescribe.end())
        {
          std::string tagGotten;
          tagGotten = srv.response.predictions.label[0];
          seft_pipeline::cluster_data cluster_msg;
          cluster_msg.cloud = clusterVector.at(i);
          cluster_msg.label = srv.response.predictions.label[0];
          cluster_msg.n = sendCounter;
          cluster_msg.image = *msg2;
          cluster_ind_pub_.publish(cluster_msg);

          //guardar imagen
          /*   std::stringstream imageName;
          imageName << "/home/jcarlos2289/catkin_ws/results/seft_pipeline/" << tagGotten << "/img/capture_" << srv.response.predictions.label[0] << "_" << sendCounter << ".jpg";
          cv::imwrite(imageName.str(), cutImage);*/

          std::stringstream tagData;
          for (int j = 0; j < srv.response.predictions.label.size(); j++)
            tagData << srv.response.predictions.label[j] << "-" << srv.response.predictions.confidence[j] << std::endl;

          std::stringstream tagFileName;
          tagFileName << "/home/jcarlos2289/catkin_ws/results/seft_pipeline/" << srv.response.predictions.label[0] << "/tags/capture_" << srv.response.predictions.label[0] << "_" << sendCounter << ".txt";

          std::string name = tagFileName.str();
          const char *nm = name.c_str();
          std::ofstream fs(nm, std::ofstream::out); //"/home/jcarlos2289/catkin_ws/results/seft_pipeline/esf_.txt"

          fs << tagData.str() << std::endl;
          fs.close();

          //std::string tdata = tagData.str();

          //saveData(sendCounter, clusterVector.at(i), cutImage, tdata, tagGotten);

          sendCounter++;
          std::cout << "-----------------------------Data of  " << srv.response.predictions.label[0] << " saved-----------------------------------------------" << std::endl;

          std::string tdata = tagData.str();

          //saveData(sendCounter, clusterVector.at(i), cutImage, tdata, tagGotten);
        }
        else
        {
          std::cout << "The label " << srv.response.predictions.label[0] << " isn't in the list" << std::endl;
        }
      }
      catch (cv::Exception ex)
      {
        ROS_ERROR("CV::Exception caught =( Filling Black/White.");
      }
    }
  }

} // callback end

void connectCallback(const ros::SingleSubscriberPublisher &pub)
{
 // std::cout << "New subscriber linked" << std::endl;
  if (cluster_ind_pub_.getNumSubscribers() == 1)
  {
    // Subscribe to camera
    rgbdimage_sub_ = nh_ptr_->subscribe<sensor_msgs::PointCloud2>("depth_registered", 1, rgbdImageCallback); //1  /camera/cloud/points ??
    ROS_INFO("New Subcriber\nWaiting for a point cloud to Starting predictions stream.");
    camera_sub = nh_ptr_->subscribe("camera_info", 1, cameraInfoCallBack);
  }
}

void disconnectCallback(const ros::SingleSubscriberPublisher &pub)
{
  //std::cout <<"The subscriber has closed the conection" << std::endl;
  if (cluster_ind_pub_.getNumSubscribers() == 0)
  {
    // Unsubscribe to camera
    //  rgbdimage_sub_.shutdown ();
    ROS_INFO("Stopping predictions stream.");
  }
}

int main(int argc, char **argv)
{

 /* if (argc == 1)
  {
    object = argv[1];
    std::cout << "Searching for: " << object << std::endl;
  }
  else
  {
    object = "";
  }*/

  // ROS init
  ros::init(argc, argv, "otg_cloud_clusterer");
  nh_ptr_ = boost::make_shared<ros::NodeHandle>();
  sendCounter = 0;

  // Advertise prediction service for batch processing
  cluster_ind_pub_ = nh_ptr_->advertise<seft_pipeline::cluster_data>("/seft/cluster/", 0,connectCallback, disconnectCallback);
  //cluster_pub_ = nh_ptr_->advertise<seft_pipeline::cluster_clouds>("/cluster_data_topic", 0, connectCallback, disconnectCallback);
  client = nh_ptr_->serviceClient<seft_pipeline::tagService>("tag_Service");

  // Subscribe to RGB-D image (moved to connect callback)
  // rgbdimage_sub_ = nh_ptr_->subscribe<sensor_msgs::PointCloud2> ("/camera/cloud/points", 1, rgbdImageCallback);
  ROS_INFO("Waiting for a subscriber.");
  // Return control to ROS
  ros::spin();

  return 0;
}
