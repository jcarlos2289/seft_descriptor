#include <ros/ros.h>
#include "ros/package.h"
//#include <geometry_msgs/Point.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/PointCloud2.h>
//#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
//#include <pcl/filters/extract_indices.h>
//#include <pcl/filters/voxel_grid.h>
//#include <pcl/features/normal_3d.h>
//#include <pcl/kdtree/kdtree.h>
//#include <pcl/sample_consensus/method_types.h>
//#include <pcl/sample_consensus/model_types.h>
//#include <pcl/segmentation/sac_segmentation.h>
//#include <pcl/segmentation/extract_clusters.h>
//#include <pcl/common/centroid.h>
#include "seft_pipeline_test/cluster_clouds.h"
//#include "seft_pipeline/tagService.h"
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <limits>
#include <pcl/conversions.h>
#include <image_transport/image_transport.h>
#include <opencv/cv.h>
//#include <image_geometry/pinhole_camera_model.h>
//#include <tf/transform_listener.h>
#include <boost/foreach.hpp>
#include <sensor_msgs/image_encodings.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/esf.h>
#include "seft_pipeline_test/cluster_data.h"
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_types.h>
#include <pcl/features/vfh.h>
#include <iostream>
#include <fstream>
#include <iostream>
#include <fstream>
#include <sensor_msgs/Image.h>
#include <exception>
#include <pcl/exceptions.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

//ros::Publisher cluster_pub_;
ros::Subscriber cluster_sub_;
boost::shared_ptr<ros::NodeHandle> nh_ptr_;
ros::ServiceClient client;
ros::Subscriber camera_sub;

void clusterCallback(seft_pipeline_test::cluster_data in_data)
{

  // Convert Point Cloud
  pcl::PCLPointCloud2 pc2_cloud;
  pcl_conversions::toPCL(in_data.cloud, pc2_cloud);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::fromPCLPointCloud2(pc2_cloud, *cloud);

  //guardar cluster

  int i = in_data.n;

  std::stringstream counter;
  counter << i;

  std::cout << "Counter Value Received: " << counter.str() << std::endl;

  pcl::PCDWriter writer;
  std::stringstream clusterName;
  clusterName << "/home/jcarlos2289/catkin_ws/results/seft_pipeline_test/" << in_data.label << "/pointcloud/cluster_" << in_data.label << "_" << i << ".pcd";

  try
  {
    writer.write<pcl::PointXYZRGB>(clusterName.str(), *cloud, false); //se guarda la nube en la carpeta
  }
  catch (pcl::PCLException &e)
  {
  }
  //guardar imagen
  std::stringstream imageName;
  imageName << "/home/jcarlos2289/catkin_ws/results/seft_pipeline_test/" << in_data.label << "/img/capture_" << in_data.label << "_" << i << ".jpg";
  try
  {
    cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(in_data.image, sensor_msgs::image_encodings::BGR8);

    cv_ptr->encoding = sensor_msgs::image_encodings::BGR8;
    cv::Mat img = cv_ptr->image;

    cv::imwrite(imageName.str(), img);
    ROS_INFO("************************************************ Cloud/Image Saved.");
  }
  catch (cv_bridge::Exception &e)
  {
    std::cout << e.what() << std::endl;
    ROS_ERROR("Could not do whatever I had to do. =(");
    // res.tag = "<------------------>";
  }

  // Object for storing the ESF descriptor.
  pcl::PointCloud<pcl::ESFSignature640>::Ptr descriptor(new pcl::PointCloud<pcl::ESFSignature640>);
  // ESF estimation object.
  pcl::ESFEstimation<pcl::PointXYZRGB, pcl::ESFSignature640> esf;
  esf.setInputCloud(cloud);
  esf.compute(*descriptor);
  std::stringstream esfData;

  for (int x = 0; x < descriptor->points[0].descriptorSize(); x++)
  {
    esfData << descriptor->points[0].histogram[x] << std::endl;
  }

  std::stringstream fileName;
  fileName << "/home/jcarlos2289/catkin_ws/results/seft_pipeline_test/" << in_data.label << "/ESF_txt/esf_" << in_data.label << "_" << i << ".txt";
  std::string name = fileName.str();
  const char *nm = name.c_str();
  std::ofstream fs(nm, std::ofstream::out);
  fs << esfData.str() << std::endl;
  fs.close();
  std::stringstream ss;
  ss << "/home/jcarlos2289/catkin_ws/results/seft_pipeline_test/" << in_data.label << "/ESF/esf_" << in_data.label << "_" << i << ".pcd";

  try
  {
    pcl::io::savePCDFile(ss.str(), *descriptor, true);
    //ROS_INFO("ESF Features Saved.");
  }
  catch (pcl::PCLException &e)
  {
  }

  /*
  // Create the normal estimation class, and pass the input dataset to it
  pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
  ne.setInputCloud(cloud);
  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());
  ne.setSearchMethod(tree);
  // Output datasets
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
  ne.setRadiusSearch(0.03);
  ne.compute(*cloud_normals);

  // Create the VFH estimation class, and pass the input dataset+normals to it
  pcl::VFHEstimation<pcl::PointXYZRGB, pcl::Normal, pcl::VFHSignature308> vfh;
  vfh.setInputCloud(cloud);
  vfh.setInputNormals(cloud_normals);
  vfh.setSearchMethod(tree);

  pcl::PointCloud<pcl::VFHSignature308>::Ptr vfhs(new pcl::PointCloud<pcl::VFHSignature308>());
  vfh.compute(*vfhs);
  std::stringstream s2;
  s2 << "/home/jcarlos2289/catkin_ws/results/seft_pipeline/" << in_data.label << "/VFH/vfh_" << in_data.label << "_" << i << ".pcd";
  std::stringstream s2Nm;
  s2Nm << "/home/jcarlos2289/catkin_ws/results/seft_pipeline/" << in_data.label << "/VFH_txt/vfh_" << in_data.label << "_" << i << ".txt";
  std::stringstream vfhData;

  for (int x = 0; x < vfhs->points[0].descriptorSize(); x++)
  {
    vfhData << vfhs->points[0].histogram[x] << std::endl;
  }

  std::string nameVFH = s2Nm.str();
  const char *nmVFH = nameVFH.c_str();
  std::ofstream fileVFH(nmVFH, std::ofstream::out);

  fileVFH << vfhData.str() << std::endl;
  fileVFH.close();
  try
  {
    std::cout << s2.str() << " saved" << std::endl;
    pcl::io::savePCDFile(s2.str(), *vfhs, true);
  }
  catch (pcl::PCLException &e)
  {
  }

  ROS_INFO("************************************************ Features/Cloud/Image Saved.");

  */
}


int main(int argc, char **argv)
{
  // ROS init
  ros::init(argc, argv, "seft_test_describer");
  nh_ptr_ = boost::make_shared<ros::NodeHandle>();

  cluster_sub_ = nh_ptr_->subscribe<seft_pipeline_test::cluster_data>("/seft_test/cluster/", 1, clusterCallback);
  ROS_INFO("Waiting for a cluster.");
  // Return control to ROS
  ros::spin();

  return 0;
}
