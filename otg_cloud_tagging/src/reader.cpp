/*
procesor receive roscaffe_msg with 
information about localization of objetcs

  Created on: jul 4 , 2016
 *      Author: jcarlos2289

*/
#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <std_msgs/String.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "otg_cloud_tagging/Prediction.h"
#include "otg_cloud_tagging/cluster_clouds.h"
#include <ros/ros.h>
#include "ros/package.h"
#include <geometry_msgs/Point.h>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include <pcl_ros/point_cloud.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <visualization_msgs/Marker.h>
#include <cmath>
#include <stdexcept> 
#include <eigen3/Eigen/Dense>
#include <pcl/io/pcd_io.h>


ros::Subscriber sub;
ros::Subscriber cluster_sub;
ros::Publisher pub;
ros::Publisher pub_rviz;
ros::Publisher vis_cent_pub;
ros::Publisher vis_tag_pub;
ros::Publisher tag_pub;
ros::Publisher pub_cluster_rviz;
ros::Publisher pub_reader_results;   

std::vector<std::vector<double> >global_confidences ;
std::vector<std::vector<std::string> >global_labels ;
std::vector<geometry_msgs::Point> global_position  ;
std::vector<otg_cloud_tagging::Prediction> global_prediction_vector;
  
std::vector<geometry_msgs::Point> global_centroids_position;
std::vector<pcl::PointCloud<pcl::PointXYZ> > global_clusterVector;
pcl::PointCloud<pcl::PointXYZ>::Ptr global_cloud;

int global_predictionNum, global_locationsNum, global_tagsAmount, globalClusterAmount, cloudsProcessed;

typedef struct {
    double r,g,b;
} COLOUR;

COLOUR GetColour(double v,double vmin,double vmax)
{
   COLOUR c = {1.0,1.0,1.0}; // white
   double dv;

   if (v < vmin)
      v = vmin;
   if (v > vmax)
      v = vmax;
   dv = vmax - vmin;

   if (v < (vmin + 0.25 * dv)) {
      c.r = 0;
      c.g = 4 * (v - vmin) / dv;
   } else if (v < (vmin + 0.5 * dv)) {
      c.r = 0;
      c.b = 1 + 4 * (vmin + 0.25 * dv - v) / dv;
   } else if (v < (vmin + 0.75 * dv)) {
      c.r = 4 * (v - vmin - 0.5 * dv) / dv;
      c.b = 0;
   } else {
      c.g = 1 + 4 * (vmin + 0.75 * dv - v) / dv;
      c.b = 0;
   }

   return(c);
}

double CalculateEuclideanDistance(geometry_msgs::Point p1, geometry_msgs::Point p2)
{	
	double diffx = p1.x - p2.x;
	double diffy = p1.y - p2.y;
    double diffz = p1.z - p2.z;
	double diffx_sqr = pow(diffx,2);
	double diffy_sqr = pow(diffy,2);
    double diffz_sqr = pow(diffz,2);
	double distance = sqrt (diffx_sqr + diffy_sqr +diffz_sqr);

return distance;
}

std::string printXYZ(geometry_msgs::Point point){
    std::stringstream ss;
    
    ss << point.x <<", " << point.y <<", " <<point.z ;
    return ss.str();
}

float mahalanobisDistance(geometry_msgs::Point pointData,  pcl::PointCloud<pcl::PointXYZ> clusterData){
    
    int numPoints= clusterData.points.size();
    Eigen:: MatrixXf point(3,1);
    point << pointData.x,pointData.y,pointData.z; 
    //media
    geometry_msgs::Point mean;
    
    mean.x =0;
    mean.y =0;
    mean.z =0;
    
    for(int i = 0; i< numPoints; ++i){
        mean.x += clusterData.points.at(i).x/numPoints;
        mean.y += clusterData.points.at(i).y/numPoints;
        mean.z += clusterData.points.at(i).z/numPoints;
     }

    
    //varianza
    geometry_msgs::Point variance;
    
    variance.x =0;
    variance.y =0;
    variance.z =0;
    
    
     for(int i = 0; i< numPoints; ++i){
        variance.x += pow(clusterData.points.at(i).x - mean.x,2)/(numPoints-1);
        variance.y += pow(clusterData.points.at(i).y - mean.y,2)/(numPoints-1);
        variance.z += pow(clusterData.points.at(i).z - mean.z,2)/(numPoints-1);
     }
    
    //Standard Deviation
    geometry_msgs::Point sd;
    
    sd.x = sqrt(variance.x);
    sd.y = sqrt(variance.y);
    sd.z = sqrt(variance.z);
        
    //covariance 
    float cvXY = 0;
    float cvXZ = 0;
    float cvYZ = 0;
    
    for(int i =0 ; i< numPoints; ++i){
        cvXY += ((clusterData.points.at(i).x - mean.x) * (clusterData.points.at(i).y - mean.y)) / numPoints;
        cvXZ += ((clusterData.points.at(i).x - mean.x) * (clusterData.points.at(i).z - mean.z)) / numPoints;
        cvYZ += ((clusterData.points.at(i).y - mean.y) * (clusterData.points.at(i).z - mean.z)) / numPoints;
     }
        
    Eigen::Matrix3f covarianceMatrix ;
    covarianceMatrix  << variance.x , cvXY , cvXZ 
                        ,cvXY , variance.y , cvYZ 
                        ,cvXZ , cvYZ , variance.z ;
    
      
    Eigen::Matrix3f inverseMatrix ;
    inverseMatrix = covarianceMatrix.inverse();
    
    Eigen:: MatrixXf pointTranspose(3,1);
    Eigen:: MatrixXf meanVector(3,1);
      
    meanVector << mean.x, mean.y, mean.z;
   
    pointTranspose=point -meanVector;
    pointTranspose.transposeInPlace();
             
    Eigen:: MatrixXf maha;
    maha  = (pointTranspose * inverseMatrix * (point -meanVector));
    
    float mahalanobis = sqrt(maha.coeff(0,0));
    
    return mahalanobis;
    
}


bool   pairCompare (const std::pair<float, std::string>& lhs,
                   const std::pair<float, std::string>& rhs)
      {
        return lhs.first > rhs.first;
      }

void print_tagging_information(){
  std::stringstream fullLine;
  //global_centroids_position 
  //global_prediction_vector


  for(int i = 0; i< globalClusterAmount; ++i){
    
    fullLine  << cloudsProcessed <<"\t" 
              << i <<"\t" 
              << global_centroids_position.at(i).x <<"\t" 
              << global_centroids_position.at(i).y <<"\t" 
              << global_centroids_position.at(i).z; 

    std::stringstream tagList;
    for(int j = 0; j < global_prediction_vector.at(i).n; ++j){
      tagList  <<"\t" <<global_prediction_vector.at(i).label.at(j) <<"\t" <<global_prediction_vector.at(i).confidence.at(j);  
    }

    fullLine << tagList.str() << std::endl;
  }

   std::ofstream fs("/home/jcarlos2289/catkin_ws/results/otg_cloud_tagging/Predictions.txt", std::ofstream::out|std::ofstream::app); 

   // Enviamos una cadena al fichero de salida:
   fs << fullLine.str() << std::endl;
   // Cerrar el fichero, 
   // para luego poder abrirlo para lectura:
   fs.close();


}




void drawInfo(){
    
     visualization_msgs::Marker markerLabel, markerCent;
     markerLabel.header.frame_id = "camera_rgb_optical_frame";
     markerLabel.header.stamp = ros::Time();
     
     markerCent.header.frame_id = "camera_rgb_optical_frame";
     markerCent.header.stamp = ros::Time();
        
    
    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    markerLabel.ns = "tags_space";
    markerCent.ns = "cent_space";
   
    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    markerLabel.type = 9;//shape; //9-> text
    //marker.text = global_labels.at(0);
    
    markerCent.type =9;// shape; //9-> text
  
    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    markerLabel.action = visualization_msgs::Marker::ADD;
    markerCent.action = visualization_msgs::Marker::ADD;

    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    markerLabel.pose.orientation.x = 0.0;
    markerLabel.pose.orientation.y = 0.0;
    markerLabel.pose.orientation.z = 0.0;
    markerLabel.pose.orientation.w = 1.0;
    
    markerCent.pose.orientation.x = 0.0;
    markerCent.pose.orientation.y = 0.0;
    markerCent.pose.orientation.z = 0.0;
    markerCent.pose.orientation.w = 1.0;

    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    markerLabel.scale.x = 0.05;
    markerLabel.scale.y = 0.05;
    markerLabel.scale.z = 0.01; //to define the font size
    
    markerCent.scale.x = 0.05;
    markerCent.scale.y = 0.05;
    markerCent.scale.z = 0.01; //to define the font size

    // Set the color -- be sure to set alpha to something non-zero!
    markerLabel.color.r = 1.0f;
    markerLabel.color.g = 1.0f;
    markerLabel.color.b = 1.0f;
    markerLabel.color.a = 1.0;
    
    markerCent.color.r = 143.0f;
    markerCent.color.g = 143.0f;
    markerCent.color.b = 143.0f;
    markerCent.color.a = 1.0;

    markerLabel.lifetime = ros::Duration();
    markerCent.lifetime = ros::Duration();
    
    pcl::PointCloud<pcl::PointXYZRGBA> cloud_ram;
    pcl::PointXYZRGBA point_ram;
        
    int idCount =0;
    for(int i = 0; i< globalClusterAmount; ++i){
       std::stringstream tagList, idStr;
       tagList << i;
      for(int j = 0; j < global_prediction_vector.at(i).n; ++j){
        tagList <<"\n" <<global_prediction_vector.at(i).confidence.at(j) <<"\t" <<global_prediction_vector.at(i).label.at(j);  
       }
       
       std::cout <<"Labels" << std::endl <<tagList.str() <<std::endl;
       
       idStr << i;
      markerCent.id = idCount;
      markerCent.pose.position.x = global_centroids_position.at(i).x;//
      markerCent.pose.position.y = global_centroids_position.at(i).y;//
      markerCent.pose.position.z = global_centroids_position.at(i).z; //
      markerCent.text = idStr.str(); 
      
      
      ++idCount;
      markerLabel.id = idCount;
      markerLabel.pose.position.x = global_centroids_position.at(i).x;//
      markerLabel.pose.position.y = global_centroids_position.at(i).y;//
      markerLabel.pose.position.z = global_centroids_position.at(i).z; //
      markerLabel.text = tagList.str();    
        
        
      vis_tag_pub.publish(markerLabel); 
      vis_cent_pub.publish(markerCent);
        
        
    }//end Main for
    
     ROS_INFO("Makers Published");
    //ros::shutdown();
}



void cluster_cb (otg_cloud_tagging::cluster_clouds input){
     cloudsProcessed++;
     ROS_INFO("Message received from /cluster_data_topic");
     std::stringstream ss;
     
     int clusterNum = input.n;
     std::vector<geometry_msgs::Point> centroids_position = input.centroids;
     
     //obtengo la nube en formato de pcl
    pcl::PCLPointCloud2 pc2_cloud;
    pcl_conversions::toPCL(input.cloud, pc2_cloud);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromPCLPointCloud2(pc2_cloud,*cloud);


    std::vector<pcl::PointCloud<pcl::PointXYZ> > clusterVector;
   
    for ( int i = 0; i < input.clusters.size(); ++i){
        pcl::PCLPointCloud2 pc2_cloud_ram;
        pcl_conversions::toPCL(input.clusters.at(i), pc2_cloud_ram);

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ram (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::fromPCLPointCloud2(pc2_cloud_ram,*cloud_ram);
        clusterVector.push_back(*cloud_ram);
        
    }
        
     
     ss<< "Number of clusters detected:  " << input.clusters.size() << std::endl;
     std::cout << ss.str();
     
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr colorCloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_total (new pcl::PointCloud<pcl::PointXYZRGBA>);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZRGBA>);
     
            
     copyPointCloud(*cloud, *colorCloud);
     double max = clusterVector.size() +1;
     
     COLOUR col = GetColour(1.0,1.0/max,max/max);
     for(int i =0; i<colorCloud->points.size(); ++i)
     {
         colorCloud->points.at(i).r = col.r*255;
         colorCloud->points.at(i).g = col.g*255;
         colorCloud->points.at(i).b = col.b*255;
         colorCloud->points.at(i).a = 100;
     }
     
    *cloud_total = *colorCloud; //nube original
     
     //double max = clusterVector.size();
     for(int i = 0; i< clusterVector.size(); ++i){
          pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cluster_ram (new pcl::PointCloud<pcl::PointXYZRGBA>);
         copyPointCloud(clusterVector.at(i), *cluster_ram);
         
         COLOUR col2 = GetColour(i/max,1.0/max,max/max);
          for(int j =0; j<cluster_ram->points.size(); ++j)
            {
                cluster_ram->points.at(j).r = col2.r*255;
                cluster_ram->points.at(j).g = col2.g*255;
                cluster_ram->points.at(j).b = col2.b*255;
                cluster_ram->points.at(j).a = 255;
            }
         
         if(i != 0)   
            *cloud_cluster +=  *cluster_ram;
           else
           *cloud_cluster =  *cluster_ram;
     }
      
        
     pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_ram (new pcl::PointCloud<pcl::PointXYZRGBA>);
     *cloud_ram = *cloud_cluster + *cloud_total;
     
     std::stringstream st;
     pcl::PCDWriter writer;
     st << "/home/jcarlos2289/catkin_ws/results/otg_cloud_tagging/cloud_cluster.pcd";
    
     writer.write<pcl::PointXYZRGBA> (st.str (), *cloud_ram, false); //se guarda la nube en la carpeta
         
      
     sensor_msgs::PointCloud2 cloud_msg;
     pcl::toROSMsg(*cloud_total,cloud_msg);  //nube original
     pub_rviz.publish(cloud_msg);
     
     sensor_msgs::PointCloud2 cloud_cluster_msg;
     pcl::toROSMsg(*cloud_cluster,cloud_cluster_msg);  //nube de clusters 
     cloud_cluster_msg.header.frame_id="camera_rgb_optical_frame";
     
     pub_cluster_rviz.publish(cloud_cluster_msg);
     
     global_centroids_position.swap(centroids_position);
     global_clusterVector.swap(clusterVector);
     global_cloud = cloud;
     global_prediction_vector = input.predictions;
     globalClusterAmount = input.n;
     
    // findNearPoints();
    drawInfo();
    print_tagging_information();
     
}





int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "otg_msg_reader");
  boost::shared_ptr<ros::NodeHandle> nh_ptr_;
  
  nh_ptr_ = boost::make_shared<ros::NodeHandle> ();

  // Create a ROS subscriber for the input point cloud
  // topicName, queueSize, callbackFunction 
  
  //sub = nh_ptr_->subscribe ("/roscaffe/localized_predictions", 1, prediction_cb);
  ROS_INFO("Waiting for data from  /cluster_data_topic.");
  cluster_sub = nh_ptr_->subscribe("/cluster_data_topic", 1, cluster_cb);
  pub_rviz = nh_ptr_->advertise<sensor_msgs::PointCloud2>("/cloud_processed",1);
  vis_cent_pub = nh_ptr_->advertise<visualization_msgs::Marker>( "cent_marker", 0 );
  vis_tag_pub = nh_ptr_->advertise<visualization_msgs::Marker>( "tag_marker", 0 );
  tag_pub  = nh_ptr_->advertise<sensor_msgs::PointCloud2>("/tags_location",0);
  pub_cluster_rviz = nh_ptr_->advertise<sensor_msgs::PointCloud2>("/clusters_processed",0);
  pub_reader_results= nh_ptr_->advertise<std_msgs::String>("/cluster_tag_location",0);
  cloudsProcessed = 0;
  std::ofstream fs("/home/jcarlos2289/catkin_ws/results/otg_cloud_tagging/Predictions.txt");
  fs <<"Capture#" <<"\t" << "Cluster#"<<"\t" << "Centroid.X" <<"\t" << "Centroid.Y"  <<"\t" << "Centroid.Z" 
     <<"\t" <<"Tag#1" <<"\t" << "Prob#1" 
     <<"\t" <<"Tag#2" <<"\t" << "Prob#2"
     <<"\t" <<"Tag#3" <<"\t" << "Prob#3" 
     <<"\t" <<"Tag#4" <<"\t" << "Prob#4"
     <<"\t" <<"Tag#5" <<"\t" << "Prob#5"
     <<std::endl;

   fs.close();
 
  // Spin
   ros::spin();
 
}

