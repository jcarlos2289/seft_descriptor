#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/String.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "Classifier.h"
#include "cloud_tagging/tagService.h"
#include "cloud_tagging/Prediction.h"


Classifier* classifier;
std::string model_path;
std::string weights_path;
std::string mean_file;
std::string label_file;
std::string image_path;

//ros::Publisher gPublisher;

std::string publishRet(const std::vector<Prediction>& predictions);

bool classify(cloud_tagging::tagService::Request  &req, cloud_tagging::tagService::Response &res)
{
    try {
        cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(req.image, "bgr8");
        //cv::imwrite("rgb.png", cv_ptr->image);
        cv_ptr->encoding = "bgr8";
		    cv::Mat img = cv_ptr->image;
    		std::vector<Prediction> predictions = classifier->Classify(img,5);
    		//cloud_tagging::Prediction msg;
            //msg.header = input->header;
        res.predictions.n = predictions.size ();
        for (std::vector<Prediction>::iterator it = predictions.begin (); it != predictions.end (); ++it)
          {
              res.predictions.confidence.push_back ((*it).second);//confidence
              res.predictions.label.push_back ((*it).first); //label
          }
    
          //res.prediction = msg;
        ROS_INFO("sending back response =) ");
    } catch (cv_bridge::Exception& e) {
        std::cout << e.what() <<std::endl;
        ROS_ERROR("Could not do whatever I had to do. =(");
       // res.tag = "<------------------>";
    }
    
  return true;
}


std::string publishRet(const std::vector<Prediction>& predictions)  {
    //std_msgs::String msg;
    std::stringstream ss;
   
    for (size_t i = 0; i < predictions.size(); ++i) {
        Prediction p = predictions[i];
        ss << "[" << p.second << " - " << p.first << "]";
    }
    //msg.data = ss.str();
    return(ss.str());
}



int main(int argc, char **argv)
{
    // Check arguments
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " <model_id> [<args>" << std::endl;
    return 1;
  }
  
  // Caffe classifier files
  std::string pkg_path = "/home/jcarlos2289/perception_ws/src/roscaffe"; //ros::package::getPath("roscaffe");
  ROS_INFO ("Load model %s from %s/models/", argv[1], pkg_path.c_str ());
  
  std::string model_id = argv[1];
  std::string model_file = pkg_path + "/models/" + model_id + "/deploy.prototxt";
  std::string trained_file = pkg_path + "/models/" + model_id + "/" + model_id + ".caffemodel";
  std::string mean_file = pkg_path + "/models/imagenet_mean.binaryproto";
  std::string label_file = pkg_path + "/models/synset_words.txt";
  
  if (!boost::filesystem::exists (model_file) ||
      !boost::filesystem::exists (trained_file) ||
      !boost::filesystem::exists (mean_file) ||
      !boost::filesystem::exists (label_file))
  {
    ROS_ERROR ("%s model not found. Please, check that the following files exist:", model_id.c_str ());
    ROS_ERROR (" - %s", model_file.c_str ());
    ROS_ERROR (" - %s", trained_file.c_str ());
    ROS_ERROR (" - %s", mean_file.c_str ());
    ROS_ERROR (" - %s", label_file.c_str ());
    ROS_ERROR ("You can also use the get_model.sh script in '%s' to download caffe pretrained models.", (pkg_path + "/models/").c_str ());
    return 1;
  }
      
    
  ros::init(argc, argv, "classifier_server");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("tag_Service", classify);
  
  //setup classifier parameters
  
  /*
   model_file =  "/home/jcarlos2289/caffe/models/bvlc_googlenet/deploy.prototxt";
   trained_file =  "/home/jcarlos2289/caffe/models/bvlc_googlenet/bvlc_googlenet.caffemodel";
   mean_file =  "/home/jcarlos2289/caffe/data/ilsvrc12/imagenet_mean.binaryproto";
   label_file =  "/home/jcarlos2289/caffe/data/ilsvrc12/synset_words_cut.txt";*/
  
  classifier = new Classifier(model_file, trained_file, mean_file, label_file);
  ROS_INFO("Ready to tag an image.");
  ros::spin();
  delete classifier;
  ros::shutdown();
  return 0;
}