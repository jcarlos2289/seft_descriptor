#include <ros/ros.h>
#include "ros/package.h"
#include <geometry_msgs/Point.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/Image.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include "cloud_tagging/cluster_clouds.h"
#include "cloud_tagging/tagService.h"
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <limits>
#include <pcl/conversions.h>
#include <image_transport/image_transport.h>
#include <opencv/cv.h>
#include <image_geometry/pinhole_camera_model.h>
#include <tf/transform_listener.h>
#include <boost/foreach.hpp>
#include <sensor_msgs/image_encodings.h>

ros::Publisher cluster_pub_;
ros::Subscriber rgbdimage_sub_;
boost::shared_ptr<ros::NodeHandle> nh_ptr_;
ros::ServiceClient client;
sensor_msgs::CameraInfo camInfo;
ros::Subscriber camera_sub;

void cameraInfoCallBack(const sensor_msgs::CameraInfoConstPtr& input){

 camInfo = *input;
 ROS_INFO("Camera Parameters Saved");
 camera_sub.shutdown();

} //sensor_msgs::PointCloud2ConstPt

// Single RGB-D image processing
void rgbdImageCallback (const sensor_msgs::PointCloud2ConstPtr& in_cloud)
{
    
  ROS_INFO ("Received point cloud.");
  //camera_sub = nh_ptr_->subscribe("/camera/rgb/camera_info", 1, cameraInfoCallBack);
  //rgbdimage_sub_.shutdown();
   
  // Convert Point Cloud
  pcl::PCLPointCloud2 pc2_cloud;
  pcl_conversions::toPCL(*in_cloud, pc2_cloud);
  
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudNoNaNs (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::fromPCLPointCloud2(pc2_cloud,*cloud);

  // Convert RGB image
  sensor_msgs::ImagePtr in_img (new sensor_msgs::Image);
  pcl::toROSMsg(*in_cloud, *in_img);
  cv_bridge::CvImageConstPtr imgOriginal = cv_bridge::toCvShare(in_img, sensor_msgs::image_encodings::BGR8);
  /// cv::imwrite( "/home/jcarlos2289/Documentos/GridedImages/Original.jpg", imgOriginal->image );

  std::vector<int> indices;
  pcl::removeNaNFromPointCloud(*cloud,*cloudNoNaNs, indices);
  
  //--------------------------Clustering procedure---------------------------------
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZRGB>);
  std::cout << "PointCloud before filtering has: " << cloud->points.size () << " data points." << std::endl; //*

  // Create the filtering object: downsample the dataset using a leaf size of 1cm
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);
  
  pcl::VoxelGrid<pcl::PointXYZRGB> vg;
  vg.setInputCloud (cloud);
  vg.setLeafSize (0.005f, 0.005f, 0.05f);//0.01f
  vg.filter (*cloud_filtered);
  std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl; //*
  
  //cloud_filtered = cloud;
  //cloud_filtered= cloudNoNaNs;
  
  //cloud_filtered->width = cloud->width*cloud->height;
  //cloud_filtered->height = 1;
  // Create the segmentation object for the planar model and set all the parameters
  pcl::SACSegmentation<pcl::PointXYZRGB> seg;
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZRGB> ());
  pcl::PCDWriter writer;
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setMaxIterations (100);
  seg.setDistanceThreshold (0.02);

  int i=0, nr_points = (int) cloud_filtered->points.size ();
  while (cloud_filtered->points.size () > 0.15 * nr_points)
  {
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (cloud_filtered);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0)
    {
      std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
      break;
    }

    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers);
    extract.setNegative (false);

    // Get the points associated with the planar surface
    extract.filter (*cloud_plane);
    std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_f);
    *cloud_filtered = *cloud_f;
  }

  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
  tree->setInputCloud (cloud_filtered);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
  ec.setClusterTolerance (0.015); // 2cm
  ec.setMinClusterSize (100);
  ec.setMaxClusterSize (25000);
  ec.setSearchMethod (tree);
  ec.setInputCloud (cloud_filtered);
  ec.extract (cluster_indices);

  std::vector< pcl::PointCloud<pcl::PointXYZRGB> > clustersDetected;

  int j = 0;
  for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
  {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZRGB>);
    
    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
      cloud_cluster->points.push_back (cloud_filtered->points[*pit]); //*
    
    cloud_cluster->width = cloud_cluster->points.size ();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;
    clustersDetected.push_back(*cloud_cluster);
    std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
    std::stringstream ss;
    ss << "/home/jcarlos2289/Documentos/clustererClouds/cloud_cluster_" << j << ".pcd";
    // writer.write<pcl::PointXYZRGB> (ss.str (), *cloud_cluster, false); //se guarda la nube en la carpeta
    j++;
  }
  std::stringstream sl;
  sl<< "Cluster Detected: " << clustersDetected.size() <<std::endl;
  std::cout << "Clustering Procedure finished !!!! " << std::endl;
  std::cout <<sl.str();
  
 //------------------------------end Clustering Procedure------------------------ 
 
 //----------------------------------centroid calculation------------------------
 
 // Create and accumulate points

std::vector<pcl::PointXYZ> centroids;
std::vector< sensor_msgs::PointCloud2> clusterVector;
for ( std::vector< pcl::PointCloud<pcl::PointXYZRGB> >::iterator it  = clustersDetected.begin(); it!= clustersDetected.end(); ++it){
    pcl::CentroidPoint<pcl::PointXYZ> centroid;
    for ( int i = 0; i < it->points.size(); ++i){
        centroid.add(
            pcl::PointXYZ (it->points[i].x,
                           it->points[i].y,
                           it->points[i].z)
                    );
    }
   pcl::PointXYZ c1;
   centroid.get (c1);
   centroids.push_back(c1);
   sensor_msgs::PointCloud2 in_img;
   pcl::toROSMsg(*it, in_img);
   clusterVector.push_back(in_img);
    
}
 std::stringstream sla;
 sla<< "Centroids Detected: " << centroids.size() <<std::endl;

std::vector<geometry_msgs::Point> positions;
 
for (int i = 0; i < centroids.size(); ++i){
    sla << "\nCluster #: " << i  << "\n" 
        << " "    << centroids.at(i).x
        << " "    << centroids.at(i).y
        << " "    << centroids.at(i).z << std::endl;
        geometry_msgs::Point p;
        p.x = centroids.at(i).x;
        p.y = centroids.at(i).y;
        p.z = centroids.at(i).z;
        positions.push_back (p);
        }
   std::cout << sla.str() <<std::endl;
 
 //------------------------------------------------------------------------------
 
 //---------------------Procesamiento de los clusters en Caffe----------------------
 cloud_tagging::tagService srv;
 std::vector<cloud_tagging::Prediction > predictionsVector; 
 //sensor_msgs::PointCloud2 <- clusterVector
 //pcl::PointCloud<pcl::PointXYZRGB> <-clustersDetected, vector
 
 std::vector<geometry_msgs::Point> positions_filtered;
 std::vector< sensor_msgs::PointCloud2> clusterVector_filtered;
 
  for(int i = 0; i < clusterVector.size(); ++i){

//-----------------**----------**----------**-------------**--------------**-----------**----------**------------------
  	
//-----------------**----------**----------**-------------**--------------**-----------**----------**------------------
       //cv_bridge::toCvShare(in_img, sensor_msgs::image_encodings::BGR8); 
      //obtener el min y max de los puntos y luego buscar con KDTree 
       //el punto maás cercano a ese en la nube raw y obtener el indice de dicho punto

       pcl::PointXYZRGB min_pt;
       pcl::PointXYZRGB max_pt;
       
       pcl::PointCloud<pcl::PointXYZRGB> ramCloud ;//(new pcl::PointCloud<pcl::PointXYZRGB>);
       ramCloud = clustersDetected.at(i);
              
       pcl::getMinMax3D(ramCloud, min_pt, max_pt); 
       
       image_geometry::PinholeCameraModel cam_model_;

       cam_model_.fromCameraInfo(camInfo);

       cv::Point3d pt_cv_min(min_pt.x, min_pt.y, min_pt.z);
       cv::Point2d uv_min;
       uv_min = cam_model_.project3dToPixel(pt_cv_min);

       cv::Point3d pt_cv_max(max_pt.x, max_pt.y, max_pt.z);
       cv::Point2d uv_max;
       uv_max = cam_model_.project3dToPixel(pt_cv_max);

       std::stringstream sd;
       sd<<"\n\nCoordinates of Min and Max in 2D\n"
         <<"Min " << uv_min.x << "  " << uv_min.y<<std::endl
         <<"Max " << uv_max.x << "  " << uv_max.y<<std::endl;
       std::cout << sd.str() <<std::endl;

//-----------------------------KDTREE-------------------------------------
   /*  pcl::KdTreeFLANN<pcl::PointXYZRGB> kdtree;
       kdtree.setInputCloud (cloud);

       int K = 1;*/



// for  min point
  /*std::vector<int> pointIdxNKNSearchMin(K);
  std::vector<float> pointNKNSquaredDistanceMin(K);

 
  if ( kdtree.nearestKSearch (min_pt, K, pointIdxNKNSearchMin, pointNKNSquaredDistanceMin) > 0 )
  {
    for (size_t i = 0; i < pointIdxNKNSearchMin.size (); ++i)
      std::cout << "    "  <<   cloud->points[ pointIdxNKNSearchMin[i] ].x 
                << " " << cloud->points[ pointIdxNKNSearchMin[i] ].y 
                << " " << cloud->points[ pointIdxNKNSearchMin[i] ].z 
                << " (squared distance: " << pointNKNSquaredDistanceMin[i] << ")" << std::endl;
  }
*/


// for max point
 /*std::vector<int> pointIdxNKNSearchMax(K); //indice del punto mas cercano al Maximo
  std::vector<float> pointNKNSquaredDistanceMax(K);


  if ( kdtree.nearestKSearch (max_pt, K, pointIdxNKNSearchMax, pointNKNSquaredDistanceMax) > 0 )
  {
    for (size_t i = 0; i < pointIdxNKNSearchMax.size (); ++i)
      std::cout << "    "  <<   cloud->points[ pointIdxNKNSearchMax[i] ].x 
                << " " << cloud->points[ pointIdxNKNSearchMax[i] ].y 
                << " " << cloud->points[ pointIdxNKNSearchMax[i] ].z 
                << " (squared distance: " << pointNKNSquaredDistanceMax[i] << ")" << std::endl;
  }*/

  //-----------------------------KDTREE-------------------------------------

       int minFadding, sizePadding;
       int cloudWidth, cloudHeight;
       minFadding = 15;
       sizePadding = 25;
       cloudWidth = cloud->width;
       cloudHeight = cloud->height;
       
       double width, height;
       std::stringstream sl;
       sl<< "Min " << min_pt.x <<"\t" << min_pt.y <<"\t" << min_pt.z <<std::endl 
         << "Max " << max_pt.x <<"\t" << max_pt.y <<"\t" << max_pt.z <<std::endl;
       width = max_pt.x - min_pt.x;  
       height = max_pt.y - min_pt.y;

       std::cout << sl.str() <<std::endl;
       if(uv_min.x > 0 && uv_min.y>0){
        positions_filtered.push_back(positions.at(i));
       	clusterVector_filtered.push_back(clusterVector.at(i));
     /*  cv::Mat ram = imgOriginal->image;
       cv::circle(ram, uv_max, 3, CV_RGB(255,0,0), -1);
       cv::circle(ram, uv_min, 3, CV_RGB(0,255,0), -1);

       cv::imwrite("/home/jcarlos2289/Documentos/GridedImages/Original.jpg", ram );*/
             
        if((uv_max.x + sizePadding)>  cloudWidth){
            width = cloudWidth -sizePadding -uv_min.x;
        }else
             width = uv_max.x - uv_min.x;  
             
        if((uv_max.y + sizePadding) > cloudHeight)
            height = cloudHeight - sizePadding -uv_min.y;
            else
              height = uv_max.y - uv_min.y;

       cv::Rect rect =  cv::Rect(uv_min.x-minFadding , uv_min.y-minFadding , width+sizePadding, height+sizePadding);
       cv::Mat cutImage = cv::Mat (imgOriginal->image, rect);
       cv::Mat copyImage = cutImage;
      
       if(width > height){
           cv::Mat ram1,ram2, outImg;
          /* cv::Size size(width +sizePadding,width +sizePadding);
           resize(copyImage,outImg,size);*/
                 
                     
           int inc = ((width+sizePadding) - (height +sizePadding))/2 ;
           if(inc <= 0){inc+=1;}
           ram1.create(inc,width+sizePadding,CV_8UC3); //rows cols 
           ram1.setTo(cv::Scalar(0,0,0)); //255,255,255
           cv::vconcat(ram1, copyImage , ram2);
           cv::vconcat(ram2, ram1 , outImg);
               
           cutImage =outImg;
         }
          
           if(width < height){
           cv::Mat ram1,ram2, outImg;
          /* cv::Size size(height +sizePadding,height +sizePadding);
           resize(copyImage,outImg,size);*/
               
           int inc =  ((height +sizePadding)-(width+sizePadding) )/2 ;
           if(inc <= 0){inc+=1;}
           ram1.create(height+sizePadding,inc,CV_8UC3); //rows cols 
           ram1.setTo(cv::Scalar(0,0,0));
           cv::hconcat(ram1, copyImage , ram2);
           cv::hconcat(ram2, ram1 , outImg);
           
           
           cutImage =outImg;
          }
             		
       std::stringstream ss;
       ss << "/home/jcarlos2289/Documentos/GridedImages/Cluster_"<<i <<".jpg";
       cv::imwrite( ss.str(), cutImage );
        //---------------------------*************************----------------------------***************************************
       // cv::imwrite( ss.str(), imgClus->image );
        //------------------------Esto esta bien se usa el srv de ROS------------------------
       
        //Esto tranforma de un cvMAT a un sensor_msg Image img->image para obtener la imagen MAT
       sensor_msgs::ImagePtr msg2;
       msg2 = cv_bridge::CvImage(std_msgs::Header(), "bgr8", cutImage).toImageMsg();
      // *msg2 es lo q enviaria a roscaffe despues de q agrego el fondo
               
              //esto si 
              srv.request.image = *msg2;
                 if (client.call(srv))
                    {
                        cloud_tagging::Prediction msgRam;
                                              
                      predictionsVector.push_back(srv.response.predictions);
                    }else
                    {
                    ROS_ERROR("Failed to call service tag_Service");
                    //return 1;
                    }
      //---------------------------*************************----------------------------***************************************               
                    
       
           
       }
   
  //---------------------------*************************----------------------------***************************************
       // cv::imwrite( ss.str(), imgClus->image );
        //------------------------Esto esta bien se usa el srv de ROS------------------------
       
       /*  //Esto tranforma de un cvMAT a un sensor_msg Image img->image para obtener la imagen MAT
       sensor_msgs::ImagePtr msg2;
       msg2 = cv_bridge::CvImage(std_msgs::Header(), "bgr8", img).toImageMsg();
       *msg2 es lo q enviaria a roscaffe despues de q agrego el fondo
       */
                 //  msg2 = cv_bridge::CvImage(std_msgs::Header(), "bgr8", cut).toImageMsg();
              
              //esto si 
             /* srv.request.image = *clus_img;
                 if (client.call(srv))
                    {
                        cloud_tagging::Prediction msgRam;
                                              
                      predictionsVector.push_back(srv.response.predictions);
                    }
                 else
                    {
                    ROS_ERROR("Failed to call service tag_Service");
                    //return 1;
                    }*/
      //---------------------------*************************----------------------------***************************************               
   }
 //---------------------------------------------------------------------------------
  
 //----------------------Sending results-----------------------------------------
 //ingresar todos los datos al objeto msg(cuertMSG)  y luego enviarlo
   
  cloud_tagging::cluster_clouds msg;
    
  msg.header.seq = in_cloud->header.seq;
  msg.header.stamp = in_cloud->header.stamp;
  msg.header.frame_id = in_cloud->header.frame_id;
  msg.n = positions_filtered.size();
  msg.cloud = *in_cloud;
  msg.centroids = positions_filtered;
  msg.clusters = clusterVector_filtered;
  msg.predictions = predictionsVector;
  
  cluster_pub_.publish (msg);
  ros::shutdown();
   
 //------------------------------------------------------------------------------
 }

void connectCallback (const ros::SingleSubscriberPublisher &pub)
{
  std::cout <<"New subscriber linked" << std::endl;
  if (cluster_pub_.getNumSubscribers() == 1)
  {
    // Subscribe to camera
    rgbdimage_sub_ = nh_ptr_->subscribe<sensor_msgs::PointCloud2> ("depth_registered", 1, rgbdImageCallback);//1  /camera/cloud/points ??
    ROS_INFO ("New Subcriber\nWaiting for a point cloud to Starting predictions stream.");
    camera_sub = nh_ptr_->subscribe("/camera/rgb/camera_info", 1, cameraInfoCallBack);
  }
}

void disconnectCallback (const ros::SingleSubscriberPublisher &pub)
{
    //std::cout <<"The subscriber has closed the conection" << std::endl;
  if (cluster_pub_.getNumSubscribers() == 0)
  {
    // Unsubscribe to camera
  //  rgbdimage_sub_.shutdown ();
    ROS_INFO ("Stopping predictions stream.");
  }
}

int main(int argc, char** argv)
{
  // ROS init
  ros::init(argc, argv, "cloud_clusterer");
  nh_ptr_ = boost::make_shared<ros::NodeHandle> ();
     
  // Advertise prediction service for batch processing
  cluster_pub_ = nh_ptr_->advertise<cloud_tagging::cluster_clouds> ("/cluster_data_topic", 1, connectCallback, disconnectCallback);
  client = nh_ptr_->serviceClient<cloud_tagging::tagService>("tag_Service");
  
  // Subscribe to RGB-D image (moved to connect callback)
 // rgbdimage_sub_ = nh_ptr_->subscribe<sensor_msgs::PointCloud2> ("/camera/cloud/points", 1, rgbdImageCallback);
  ROS_INFO("Waiting for a subscriber.");
  // Return control to ROS
  ros::spin();
  
  return 0;
}
