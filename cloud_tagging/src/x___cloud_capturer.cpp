#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sstream> // for converting the command line parameter to integer


const std::string RECEIVE_IMG_TOPIC_NAME= "camera/rgb/image_raw"; 
const std::string PUBLISH_IMG_TOPIC_NAME= "/original/rgb/image_raw"; 
image_transport::Publisher pub;
image_transport::Subscriber sub;


void imageCallback(const sensor_msgs::ImageConstPtr& msg) {
    //SE RECIVE LA IMAGEN DE LA XTION(camera/rgb/image_raw) 
    //Y SE PUBLICA EN EL TOPIC DEL /original/rgb/image_raw 
     pub.publish(msg);

      
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "cam_capturer");
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);
  pub = it.advertise(PUBLISH_IMG_TOPIC_NAME, 1);
  sub = it.subscribe(RECEIVE_IMG_TOPIC_NAME, 1, imageCallback);
  
  ros::spin();
  
}
