/*
 * image_sender.cpp
 *
 *  Created on: jun 30, 2016
 *      Author: jcarlos2289
 */

#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/String.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>


#include <cstdlib>
#include <cv.h>
#include <opencv2/core/core.hpp>
#include <highgui.h>
#include <stdio.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "scene_classifier/tagService.h"
#include <vector>



const std::string RECEIVE_IMG_TOPIC_NAME = "original/rgb/image_raw";   //de este recibe la imagen 
const std::string RECEIVE_RET_TOPIC_NAME = "/caffe_ret";  // de este recibira la etiqueta

const std::string PUBLISH_IMG_TO_CLASSIFY_TOPIC_NAME= "camera/rgb/image_raw";   //de este recibe la imagen 
const std::string PUBLISH_RET_FULL_TOPIC_NAME = "/caffe_ret";  // de este recibira la etiqueta


std::string model_path;
std::string weights_path;
std::string mean_file;
std::string label_file;
std::string image_path;

ros::Publisher gPublisher;

void publishRet();

void imageCallback(const sensor_msgs::ImageConstPtr& msg) {
  /*  try {
        cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, "bgr8");
        //cv::imwrite("rgb.png", cv_ptr->image);
        cv::Mat image = cv_ptr->image;
             
        //  image_transport::Publisher pub = it.advertise(PUBLISH_IMG_TO_CLASSIFY_TOPIC_NAME, 1);
       cv::Mat cut;
    //image = imread(path);

    // imread(argc[1], 1);
    if (image.empty()) {
        std::cout << "Could not open or find the image" << std::endl;
        //return -1;
    }
   
  //cv::Mat frame;
  sensor_msgs::ImagePtr msg;

  //ros::Rate loop_rate(1);
//  while (nh.ok()) {
    //cap >> frame;
    // Check if grabbed frame is actually full with some content


    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            cut = cv::Mat(image, cv::Rect(160*j, 120*i, 160, 120)).clone();
             if(!cut.empty()) {
                msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", cut).toImageMsg();
                gPublisher.publish(msg);
                cv::waitKey(1);
               }
             }

        }
        
    //}//end while
           
        
    
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }*/
}

// TODO: Define a msg or create a service
// Try to receive : $rostopic echo /caffe_ret


using namespace std;
using namespace cv;


int main(int argc, char **argv) {
   /* if (argc == 0){
        std::cout << "Image file missing..." << std::endl;
        return 0;
    }*/
    

    ros::init(argc, argv, "image_sender");
    string path = "/home/jcarlos2289/Escritorio/sequence1visual343.png"; //argv[1];


    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);
    // To receive an image from the topic, PUBLISH_RET_TOPIC_NAME
    //image_transport::Subscriber sub = it.subscribe(RECEIVE_IMG_TOPIC_NAME, 1, imageCallback);
    // To send an image to the claasifer node
   // gPublisher = nh.advertise<sensor_msgs::Image>(PUBLISH_IMG_TO_CLASSIFY_TOPIC_NAME, 1);
    // image_transport::Publisher pub = it.advertise(PUBLISH_IMG_TO_CLASSIFY_TOPIC_NAME, 1);

 
 
 //definir el cliente del servicio
 ros::ServiceClient client = nh.serviceClient<scene_classifier::tagService>("tag_Service");
 scene_classifier::tagService srv;   
    Mat image, cut;
    image = imread(path);

    // imread(argc[1], 1);
    if (image.empty()) {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }
   
   
   
   
   
   
  //cv::Mat frame;
  sensor_msgs::ImagePtr msg;

  //ros::Rate loop_rate(1);
  while (nh.ok()) {
    //cap >> frame;
    // Check if grabbed frame is actually full with some content
std::vector<std::string> tagList;

    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            cut = cv::Mat(image, cv::Rect(160*j, 120*i, 160, 120)).clone();
             if(!cut.empty()) {
               // msg->encoding =sensor_msgs::image_encodings::BGR8;
                msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", cut).toImageMsg();
               
               srv.request.image = *msg;
               if (client.call(srv))
                {
                      tagList.push_back(srv.response.tag);
                    //ROS_INFO("Sum: %ld", (long int)srv.response.sum);
                }
                else
                {
                    ROS_ERROR("Failed to call service tag_Service");
                    return 1;
                }
                                 // pub.publish(msg);
                cv::waitKey(1);
               }
             }

        }
        
        
        std::cout << "-------------------------Results---------------------" <<std::endl;
        std::vector<std::string>::iterator it;
        
        int o = 1;
        for(it = tagList.begin(); it!= tagList.end(); it++){
            
            
            std::cout << *it ;
            if (o%4 ==0){
                std::cout<< std::endl;
            }else
                std::cout<< "\t\t";
            
            ++o;
        }
        std::cout << "----------------------------------------------" <<std::endl;
        
    }//end while

 //ros::shutdown();

    ros::spin();
   
    ros::shutdown();
    return 0;
}
