/*
 * cloud_sender.cpp
 *
 *  Created on: jul 21, 2016
 *      Author: jcarlos2289
 */

#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/String.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <cstdlib>
#include <cv.h>
#include <opencv2/core/core.hpp>
#include <highgui.h>
#include <stdio.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "cloud_tagging/tagService.h"
#include <vector>



const std::string RECEIVE_IMG_TOPIC_NAME = "original/rgb/image_raw";   //de este recibe la imagen 

std::string model_path;
std::string weights_path;
std::string mean_file;
std::string label_file;
std::string image_path;

ros::Publisher gPublisher;
ros::ServiceClient client;

void publishRet();

void imageCallback(const sensor_msgs::ImageConstPtr& msg) {
    
    try {
        cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, "bgr8");
        cv::Mat image = cv_ptr->image;
        cv::Mat cut;
  
    if (image.empty()) {
        std::cout << "Could not open or find the image" << std::endl;
        //return -1;
    }
   
  //cv::Mat frame;
  sensor_msgs::ImagePtr msg2;
  std::vector<std::string> tagList;
  cloud_tagging::tagService srv;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            cut = cv::Mat(image, cv::Rect(160*j, 120*i, 160, 120)).clone();
             if(!cut.empty()) {
                msg2 = cv_bridge::CvImage(std_msgs::Header(), "bgr8", cut).toImageMsg();
                srv.request.image = *msg2;
                 if (client.call(srv))
                    {
                      tagList.push_back(srv.response.label.at(0));
                    }
                 else
                    {
                    ROS_ERROR("Failed to call service tag_Service");
                    //return 1;
                    }
                     
                    
               }
             }

        }
          
        std::cout << "-------------------------Results---------------------" <<std::endl;
        std::vector<std::string>::iterator it;
        
        int o = 1;
        for(it = tagList.begin(); it!= tagList.end(); it++){
               
            std::cout << *it ;
            if (o%4 ==0){
                std::cout<< std::endl;
            }else
                std::cout<< "\t\t";
            ++o;
        }
        std::cout << "----------------------------------------------" <<std::endl;
         
          cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );// Create a window for display.
                      cv::imshow( "Display window", image );  
                    
                    cv::waitKey(4000);
                    cv::destroyWindow("Display window");
                     cv::waitKey(1);
         
          
          
           
        
    
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
}


using namespace std;
using namespace cv;


int main(int argc, char **argv) {


    ros::init(argc, argv, "image_sender");
    string path = "/home/jcarlos2289/Escritorio/sequence1visual343.png"; //argv[1];
   ros::NodeHandle nh;  //---------------------------------------------------
    image_transport::ImageTransport it(nh);
   
    image_transport::Subscriber sub = it.subscribe(RECEIVE_IMG_TOPIC_NAME, 1, imageCallback);
  
 client = nh.serviceClient<cloud_tagging::tagService>("tag_Service");
 


    ros::spin();
   
    ros::shutdown();
    return 0;
}
